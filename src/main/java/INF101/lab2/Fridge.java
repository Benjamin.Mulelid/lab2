package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int maxSize = 20;
    List<FridgeItem> items = new ArrayList<FridgeItem>();

    /**
	 * Returns the number of items currently in the fridge
	 * 
	 * @return number of items in the fridge
	 */
    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    /**
	 * The fridge has a fixed (final) max size.
	 * Returns the total number of items there is space for in the fridge
	 * 
	 * @return total size of the fridge
	 */
    @Override
    public int totalSize() {
        return maxSize;
    }

    /**
	 * Place a food item in the fridge. Items can only be placed in the fridge if
	 * there is space
	 * 
	 * @param item to be placed
	 * @return true if the item was placed in the fridge, false if not
	 */
    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() == 20) {
            return false;
        } else {
            items.add(item);
            return true;
        }
    }

    /**
	 * Remove item from fridge
	 * 
	 * @param item to be removed
	 * @throws NoSuchElementException if fridge does not contain <code>item</code>
	 */
    @Override
    public void takeOut(FridgeItem item) {
        if (items.contains(item)) {
            items.remove(item);
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
	 * Remove all items from the fridge
	 */
    @Override
    public void emptyFridge() {
        items.clear();
    }

    /**
	 * Remove all items that have expired from the fridge
	 * @return a list of all expired items
	 */
    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> removedItems = new ArrayList<FridgeItem>();

        for (FridgeItem fridgeItem : items) {
            if (fridgeItem.hasExpired()) {
                removedItems.add(fridgeItem);
            }
        }
        for (FridgeItem fridgeItem : removedItems) {
            takeOut(fridgeItem);
        }

        return removedItems;
    }
    
}
